﻿using System;
using System.Numerics;

namespace Cloudeon.GeometryMath
{
    /// <summary>
    /// Triangle math
    /// </summary>
    public static class TriangleMath
    {
        /// <summary>
        /// Types of classification for triangles
        /// </summary>
        /// <remark>
        /// Isosceles uses Euclids definition of a triangle with exactly two edges of equal length
        /// </remark>
        public enum TriangleClassification
        {
            /// <summary>
            /// All three edges are of equal length
            /// </summary>
            Equilateral,

            /// <summary>
            /// Exactly two edges are of equal length (uses Euclids definition)
            /// </summary>
            Isosceles,

            /// <summary>
            /// No edges are of equal length
            /// </summary>
            Scalene
        }


        /// <summary>
        /// Determines the classification of a triangle by the length of all three edges
        /// </summary>
        /// <param name="edgeLengthA">Length of edge A</param>
        /// <param name="edgeLengthB">Length of edge B</param>
        /// <param name="edgeLengthC">Length of edge C</param>
        /// <returns>Classification of the triangle. Equilateral, Isosceles (Exactly two edges equal of equal length) or Scalene</returns>
        /// <exception cref="System.ArgumentException">Thrown when one or more edges having length equal to or less than 0</exception>
        public static TriangleClassification DetermineTriangleClassification( float edgeLengthA, float edgeLengthB, float edgeLengthC )
        {
            if ( edgeLengthA <= 0 || edgeLengthB <= 0 || edgeLengthC <= 0 )
            {
                throw new ArgumentException();
            }

            var areEqualAB = edgeLengthA == edgeLengthB;
            var areEqualBC = edgeLengthB == edgeLengthC;

            if ( areEqualAB && areEqualBC )
            {
                // All three edges are of equal length
                return TriangleClassification.Equilateral;
            }

            var areEqualAC = edgeLengthA == edgeLengthC;

            if ( areEqualAC || areEqualAB || areEqualBC )
            {
                // Exactly two edges are of equal length
                return TriangleClassification.Isosceles;
            }
            else
            {
                // None of the edges are of equal length
                return TriangleClassification.Scalene;
            }
        }


        /// <summary>
        /// Determines the classification of a triangle by the coordinate of each vertex in a 3 dimensional space
        /// </summary>
        /// <param name="coordinateA">Coordinate of A</param>
        /// <param name="coordinateB">Coordinate of B</param>
        /// <param name="coordinateC">Coordinate of C</param>
        /// <returns>Classification of the triangle. Equilateral, Isosceles (Exactly two edges equal of equal length) or Scalene</returns>
        /// <exception cref="System.ArgumentException">Thrown when two or more coordinates are equal</exception>
        public static TriangleClassification DetermineTriangleClassification( Vector3 coordinateA, Vector3 coordinateB, Vector3 coordinateC )
        {
            var edgeLengthA = Vector3.Distance( coordinateA, coordinateB );
            var edgeLengthB = Vector3.Distance( coordinateB, coordinateC );
            var edgeLengthC = Vector3.Distance( coordinateC, coordinateA );

            return DetermineTriangleClassification( edgeLengthA, edgeLengthB, edgeLengthC );
        }
    }
}
