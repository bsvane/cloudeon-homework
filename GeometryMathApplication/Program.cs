﻿using System;

namespace Cloudeon.GeometryMathApplication
{
    class Program
    {
        static void Main( string[] args )
        {
            Console.WriteLine( "Cloudeon Triangle Classifier. Use possitive floats or integers as input" );

            var goAgain = true;
            while ( goAgain )
            {
                Console.Write( "Input length of edge A: " );
                var inputEdgeLengthA = Console.ReadLine();

                Console.Write( "Input length of edge B: " );
                var inputEdgeLengthB = Console.ReadLine();

                Console.Write( "Input length of edge C: " );
                var inputEdgeLengthC = Console.ReadLine();

                var isEdgeValidA = float.TryParse( inputEdgeLengthA, out var edgeLengthA );
                var isEdgeValidB = float.TryParse( inputEdgeLengthB, out var edgeLengthB );
                var isEdgeValidC = float.TryParse( inputEdgeLengthC, out var edgeLengthC );

                var areAllInputsValid = isEdgeValidA && isEdgeValidB && isEdgeValidC;

                if ( areAllInputsValid )
                {
                    try
                    {
                        var triangleClassification = GeometryMath.TriangleMath.DetermineTriangleClassification( edgeLengthA, edgeLengthB, edgeLengthC );
                        Console.WriteLine( $"The classification of a triangle with edges of the length supplied is: {triangleClassification}" );
                    }
                    catch ( ArgumentException )
                    {
                        areAllInputsValid = false;
                    }
                }

                if ( !areAllInputsValid )
                {
                    Console.WriteLine( "All inputs must be in possitive floats or integers" );
                }

                Console.Write( "Do you want to have another go (Y/N)?" );
                var goAgainKey = Console.ReadKey();
                Console.WriteLine();

                goAgain = char.ToUpperInvariant( goAgainKey.KeyChar ) == 'Y';
            }
        }
    }
}
