using Cloudeon.GeometryMath;
using System;
using System.Collections.Generic;
using System.Numerics;
using Xunit;

namespace Cloudeon.GeometryMathUnitTests
{
    public class TriangleMathTests
    {
        [Theory]
        [InlineData( 0, 1, 1 )]
        [InlineData( 1, 0, 1 )]
        [InlineData( 1, 1, 0 )]
        [InlineData( -1, 1, 1 )]
        [InlineData( 1, -1, 1 )]
        [InlineData( 1, 1, -1 )]
        public void DetermineTriangleClassification_InvalidTriangle_ArgumentExceptionThrown( float edgeLengthA, float edgeLengthB, float edgeLengthC )
        {
            // Act and Assert
            Assert.Throws<ArgumentException>( () => TriangleMath.DetermineTriangleClassification( edgeLengthA, edgeLengthB, edgeLengthC ) );
        }


        [Fact]
        public void DetermineTriangleClassification_ValidEquilateralTriangle_ReturnEquilateralTriangleClassification()
        {
            // Arrange
            var edgeLength = 1.1f;

            // Act
            var result = TriangleMath.DetermineTriangleClassification( edgeLength, edgeLength, edgeLength );

            // Assert
            Assert.Equal( TriangleMath.TriangleClassification.Equilateral, result );
        }


        [Theory]
        [InlineData( 2.1f, 1.1f, 1.1f )]
        [InlineData( 1.1f, 2.1f, 1.1f )]
        [InlineData( 1.1f, 1.1f, 2.1f )]
        public void DetermineTriangleClassification_ValidIsoscelesTriangle_ReturnIsoscelesTriangleClassification( float edgeLengthA, float edgeLengthB, float edgeLengthC )
        {
            // Act
            var result = TriangleMath.DetermineTriangleClassification( edgeLengthA, edgeLengthB, edgeLengthC );

            // Assert
            Assert.Equal( TriangleMath.TriangleClassification.Isosceles, result );
        }


        [Fact]
        public void DetermineTriangleClassification_ValidScaleneTriangle_ReturnScaleneTriangleClassification()
        {
            // Arrange
            var edgeLengthA = 1.1f;
            var edgeLengthB = 1.2f;
            var edgeLengthC = 1.3f;

            // Act
            var result = TriangleMath.DetermineTriangleClassification( edgeLengthA, edgeLengthB, edgeLengthC );

            // Assert
            Assert.Equal( TriangleMath.TriangleClassification.Scalene, result );
        }


        public static IEnumerable<object[]> DetermineTriangleClassification_InvalidCoordinates_ArgumentExceptionThrown_Data => new List<object[]>
        {
            new object[] {Vector3.Zero, Vector3.Zero, Vector3.Zero },
            new object[] {Vector3.UnitX, Vector3.Zero, Vector3.Zero},
            new object[] {Vector3.Zero, Vector3.UnitX, Vector3.Zero},
            new object[] {Vector3.Zero, Vector3.Zero, Vector3.UnitX },
        };

        [Theory]
        [MemberData( nameof( DetermineTriangleClassification_InvalidCoordinates_ArgumentExceptionThrown_Data ) )]
        public void DetermineTriangleClassification_InvalidCoordinates_ArgumentExceptionThrown( Vector3 coordinateA, Vector3 coordinateB, Vector3 coordinateC )
        {
            // Act and Assert
            Assert.Throws<ArgumentException>( () => TriangleMath.DetermineTriangleClassification( coordinateA, coordinateB, coordinateC ) );
        }


        [Fact]
        public void DetermineTriangleClassification_ValidCoordinates_ReturnEquilateralTriangleClassification()
        {
            // Arrange
            var equilateralTriangleHeight = (float)( Math.Sqrt( 3 ) / 2.0 * Vector3.UnitX.X );

            // Act
            var result = TriangleMath.DetermineTriangleClassification( Vector3.Zero, Vector3.UnitX, new Vector3( Vector3.UnitX.X / 2.0f, equilateralTriangleHeight, 0 ) );

            // Assert
            Assert.Equal( TriangleMath.TriangleClassification.Equilateral, result );
        }


        public static IEnumerable<object[]> DetermineTriangleClassification_ValidCoordinates_ReturnIsoscelesTriangleClassification_Data => new List<object[]>
        {
            new object[] {Vector3.Zero, Vector3.UnitX, Vector3.UnitY},
            new object[] {Vector3.UnitX, Vector3.Zero, Vector3.UnitY},
            new object[] {Vector3.UnitX, Vector3.UnitY, Vector3.Zero},
        };

        [Theory]
        [MemberData( nameof( DetermineTriangleClassification_ValidCoordinates_ReturnIsoscelesTriangleClassification_Data ) )]
        public void DetermineTriangleClassification_ValidCoordinates_ReturnIsoscelesTriangleClassification( Vector3 coordinateA, Vector3 coordinateB, Vector3 coordinateC )
        {
            // Act
            var result = TriangleMath.DetermineTriangleClassification( coordinateA, coordinateB, coordinateC );

            // Assert
            Assert.Equal( TriangleMath.TriangleClassification.Isosceles, result );
        }



        [Fact]
        public void DetermineTriangleClassification_ValidCoordinates_ReturnScaleneTriangleClassification()
        {
            // Arrange
            var coordinateA = Vector3.Zero;
            var coordinateB = Vector3.UnitX;
            var coordinateC = Vector3.One;

            // Act
            var result = TriangleMath.DetermineTriangleClassification( coordinateA, coordinateB, coordinateC );

            // Assert
            Assert.Equal( TriangleMath.TriangleClassification.Scalene, result );
        }
    }
}
